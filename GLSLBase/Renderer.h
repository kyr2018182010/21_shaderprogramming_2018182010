#pragma once

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Dependencies\glew.h"
#include "Dependencies\wglew.h"
#include "Dependencies\glm/glm.hpp"
#include "Dependencies\glm/gtc/matrix_transform.hpp"
#include "Dependencies\glm/gtx/euler_angles.hpp"

class Renderer
{
public:
	Renderer(int windowSizeX, int windowSizeY);
	~Renderer();

	GLuint CreatePngTexture(char * filePath);
	GLuint CreateBmpTexture(char * filePath);

	void CreateParticle(int count);
	void Paricle();

	void FSSandBox();
	void GridMeshFSSandBox();

	// Texture
	void CreateTextures();
	void DrawSimpleTexture();
	void Test();

private:
	void Initialize(int windowSizeX, int windowSizeY);
	void CreateGridGeometry();

	bool ReadFile(char* filename, std::string *target);
	void AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType);
	GLuint CompileShaders(char* filenameVS, char* filenameFS);
	void CreateVertexBufferObjects();
	GLuint CreateFBO(int sx, int sy, GLuint* tex, GLuint* depthTex);

	unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight);

	bool m_Initialized = false;
	
	int m_WindowSizeX = 0;
	int m_WindowSizeY = 0;

	//camera position
	glm::vec3 m_v3Camera_Position;
	//camera lookat position
	glm::vec3 m_v3Camera_Lookat;
	//camera up vector
	glm::vec3 m_v3Camera_Up;

	glm::mat4 m_m4OrthoProj;
	glm::mat4 m_m4PersProj;
	glm::mat4 m_m4Model;
	glm::mat4 m_m4View;
	glm::mat4 m_m4ProjView;

	GLuint m_VBORect = 0;
	GLuint m_SolidRectShader = 0;
	GLuint VBO;
	GLuint VBO1;

	GLuint m_VBOFSSandbox = 0;
	GLuint m_FSSandboxShader = 0;


	// Particle
	int m_ParticleCount = 0;
	GLuint m_VBOParticle = -1;

	// grid mesh
	GLuint m_GridMeshShader = 0;
	GLuint m_Count_ProxyGeo = 0;
	GLuint m_VBO_ProxyGeo = -1;

	// Texture
	GLuint m_TextureCheckerBoardShader = -1;
	GLuint m_TextureCheckerBoard = 0;
	GLuint m_VBORect_PosTex = 0;
	
	GLuint m_TextureRGB = 0.f;

	GLuint m_TextureID = 0;
	GLuint m_TextureID1 = 0;
	GLuint m_TextureID2= 0;
	GLuint m_TextureID3 = 0;
	GLuint m_TextureID4 = 0;
	GLuint m_TextureID5 = 0;
	GLuint m_TextureIDTotal = 0;

	GLuint m_FBO_0;
	GLuint m_FBOTexture = 0;
	GLuint m_FBODepth = 0;
	
	GLuint m_FBO_P;
	GLuint m_FBOTexture_P = 0;
	GLuint m_FBODepth_P = 0;

	GLuint m_FBO_F;
	GLuint m_FBOTexture_F = 0;
	GLuint m_FBODepth_F = 0;

	GLuint m_FBO_G;
	GLuint m_FBOTexture_G = 0;
	GLuint m_FBODepth_G = 0;
};

