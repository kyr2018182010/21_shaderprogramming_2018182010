#version 450

layout(location=0) out vec4 FragColor; 

in vec4 v_Color;

void main()
{
	FragColor = vec4(0.5) * asin(v_Color.y - v_Color.w) * 5 + 0.3;
}
