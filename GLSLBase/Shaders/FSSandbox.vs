#version 450

in vec3 a_Position;		// 사용자가 정의한 입력 값

const vec3 Circle = vec3(0.0,0.0,0.0);
uniform float u_Size;

varying vec4 v_Color;

void main()
{
	vec3 position = a_Position * u_Size;
	gl_Position = vec4(position,1);
	v_Color = vec4(position,0);
}
