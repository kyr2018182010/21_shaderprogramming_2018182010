#version 450

layout(location=0) out vec4 FragColor; 

uniform sampler2D u_TexSampler;
uniform float u_Time;

in vec2 v_TexPos;

const float PI = 3.141592;

vec4 P1(){
	// 절대값 그래프
	vec2 newTexCoord = v_TexPos;
	newTexCoord.y = abs(newTexCoord.y -0.5) * 2.0;
	return texture(u_TexSampler,newTexCoord);
}

vec4 P2(){
	vec2 newTexCoord = v_TexPos;
	newTexCoord.x = fract(newTexCoord.x * 3.0);
	newTexCoord.y = floor(v_TexPos.x * 3.0) / 3.0 + newTexCoord.y / 3.0;		//ceil : 올림  | floor : 내림
	return texture(u_TexSampler,newTexCoord);
}

vec4 P3(){
	vec2 newTexCoord = v_TexPos;
	newTexCoord.x = fract(newTexCoord.x * 3.0);		// fract = x - floor(x)
	newTexCoord.y = floor((1-v_TexPos.x) * 3.0) / 3.0 + newTexCoord.y / 3.0;
	return texture(u_TexSampler,newTexCoord);
}

vec4 P4(){
	vec2 newTexCoord = v_TexPos;
	newTexCoord.y = (2 - floor(newTexCoord.y * 3.0))/ 3.0;		// start
	newTexCoord.y += fract(v_TexPos.y * 3.0) / 3.0;				// add 0 ~ 1/3
	return texture(u_TexSampler,newTexCoord);
}

vec4 P5(){
	vec2 newTexCoord = v_TexPos;
	newTexCoord.x = fract(newTexCoord.x * 2.0) + floor(v_TexPos.y * 2.0) * 0.5;
	newTexCoord.y = fract(newTexCoord.y * 2.0);
	return texture(u_TexSampler,newTexCoord);
}

vec4 P6(){
	vec2 newTexCoord = v_TexPos;
	newTexCoord.x = fract(newTexCoord.x * 2.0);
	newTexCoord.y = fract(newTexCoord.y * 2.0) + floor(v_TexPos.x * 2.0) * 0.5;
	return texture(u_TexSampler,newTexCoord);
}

vec4 SingleTexture(){		// texture animation
	return texture(u_TexSampler,v_TexPos);
}

vec4 SpriteTexture2(){
	vec2 newTexCoord = v_TexPos;
	newTexCoord.x = fract(v_TexPos.x * 2);
	newTexCoord.y = newTexCoord.y/2.0;
	newTexCoord.y += 0.5 * floor(v_TexPos.x * 2);
	return texture(u_TexSampler,newTexCoord);
}

vec4 SpriteTexture(){
	vec2 newTexCoord = v_TexPos;
	newTexCoord.y = v_TexPos.y/6.0 + floor(fract(u_Time) * 6.0 )/6.0;
	return texture(u_TexSampler,newTexCoord);
}

void main()
{
	//FragColor = vec4(v_TexPos,0,1);
	//FragColor =P6();
	FragColor =SpriteTexture2();
}
