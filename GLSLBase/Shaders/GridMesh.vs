#version 450

in vec3 a_Position;		// 사용자가 정의한 입력 값

uniform float u_Time;

const float PI = 3.141592;

out vec4 v_Color;

void main()
{
	vec3 position = a_Position;
	float sinValue = 2 * PI *(a_Position.x + 0.5);
	position.y = sin(sinValue + u_Time * 2 ) * 0.2 *(a_Position.x + 0.5) + a_Position.y;		// * (a_Position.x + 0.5) : 왼쪽 축 고정
	gl_Position = vec4(position,1);
	v_Color = vec4(position,a_Position.y);
}
