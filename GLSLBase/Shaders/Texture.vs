#version 450

in vec3 a_Position;		// 사용자가 정의한 입력 값
in vec2 a_TexPos;

out vec2 v_TexPos;

void main()
{
	gl_Position = vec4(a_Position,1);
	v_TexPos = a_TexPos;
}
