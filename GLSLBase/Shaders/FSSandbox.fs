#version 450

layout(location=0) out vec4 FragColor; 

varying vec4 v_Color;

const vec3 Circle = vec3(0.5,0.5,0.0);
const float PI = 3.141592;

uniform vec3 u_Point;
uniform vec3 u_Points[10];
uniform float u_Time;

vec4 CenteredCircle(){
	int count = 10;
	float d = length(v_Color.rgb - Circle);
	float rad = d * 2.0 * 2.0 * PI * count; 
	float greyScale = sin(rad);
	float width = 20;
	greyScale = pow(greyScale, width);
	return vec4(greyScale);
}

vec4 IndicatePoint(){
	vec4 returnColor = vec4(0);
	float d = length(v_Color.rg - u_Point.xy);
	if(d<u_Point.z)
	{
		returnColor = vec4(1);
	}
	return returnColor;
}

vec4 IndicatePoints(){
	vec4 returnColor = vec4(0);
	for(int i = 0 ; i< 10 ; ++i)
	{
		float d = length(v_Color.rg - u_Points[i].xy);
		if(d<u_Points[i].z)
		{
			returnColor = vec4(1) * cos((d/u_Points[i].z) * 0.5* PI);
		}
	}
	return returnColor;
}

vec4 Radar(){
	float d = length(v_Color.rg - vec2(0,0));
	vec4 returnColor = vec4(0);
	float ringRadius = mod(u_Time,0.7);
	float ringWidth = 0.015;

	if(d>ringRadius && d < ringRadius + ringWidth){
		returnColor = vec4(0.5) * (0.015-abs(ringRadius+ ringWidth * 0.5 - d)) * 60;
		//returnColor = vec4(0.5);
		for(int i = 0 ; i <10; ++i){
			float pointDistance = length(u_Points[i].xy - v_Color.rg);
			if(pointDistance < 0.05){
				pointDistance = 0.05 - pointDistance;
				pointDistance *= 20;
				returnColor += vec4(pointDistance);
			}
		}
	}
	returnColor.a = 0.8;
	return returnColor;
}

vec4 Radar2(){
	vec4 returnColor = vec4(0);
	float theta = 2 * PI + u_Time;
	float width = 0.01;
	vec2 p2 = vec2(cos(theta),sin(theta)) * 0.5;
	vec2 p = v_Color.xy;
	p.y = (p2.y/p2.x) * v_Color.x;
	float dist = length(p - v_Color.xy);

	if(dist <= width				// draw line
	&& length(p2-v_Color.xy) <= 0.5		// only draw 1 side of line
	&& length(vec2(0) - v_Color.xy) <= 0.5)	// equalize line length
	{
		returnColor += vec4(0.3) * (1 -dist/width);
		for(int i = 0 ; i <10; ++i){
			float pointDistance = length(u_Points[i].xy - v_Color.rg);
			if(pointDistance < 0.05){
				pointDistance = 0.05 - pointDistance;
				pointDistance *= 20;
				returnColor += vec4(pointDistance);
			}
		}
	}
	returnColor.a = 0.8;
	return returnColor;
}

vec4 Wave()
{
	vec4 returnColor = vec4(0);
	for(int i= 0 ; i < 10 ; ++i){
		vec2 origin = u_Points[i].xy;
		vec2 pos = v_Color.rg;
		float d = length(origin - pos);
		float preq = 8;

		returnColor += 0.4 *vec4(sin(d * 2 * PI *preq - u_Time));
	}
	//returnColor = normalize(returnColor);
	//returnColor.a = 0.5;
	return returnColor;
}

vec4 circleWave(){
	vec4 returnColor = vec4(1);
	float d = length(vec2(0,0) - v_Color.xy);
	float preq = 3;
	returnColor *= vec4(sin(d * PI * 2 * preq - u_Time));
	return returnColor;
}

void main()
{
	//FragColor = IndicatePoints();
	//FragColor = Radar();
	FragColor = Radar2();
	//FragColor = Wave();
	//FragColor = circleWave();
}
